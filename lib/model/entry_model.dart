import 'dart:math';

class Entry {
  int _entryId;
  String _entryCreated;
  String _entryTitle;
  String _entryText;

  int get entryId => _entryId;
  String get entryCreated => _entryCreated;
  String get entryTitle => _entryTitle;
  String get entryText => _entryText;

  Entry.Create(String entryTitle, String entryText) {
    _entryId = Random.secure().nextInt(50000);
    _entryCreated = DateTime.now().toString();
    _entryTitle = entryTitle;
    _entryText = entryText;
  }

  Entry(
      {int entryId, String entryCreated, String entryTitle, String entryText}) {
    _entryId = entryId;
    _entryCreated = entryCreated;
    _entryTitle = entryTitle;
    _entryText = entryText;
  }

  Entry.fromJson(dynamic json) {
    _entryId = json["entryId"];
    _entryCreated = json["entryCreated"];
    _entryTitle = json["entryTitle"];
    _entryText = json["entryText"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["entryId"] = _entryId;
    map["entryCreated"] = DateTime.now().toString();
    map["entryTitle"] = _entryTitle;
    map["entryText"] = _entryText;
    return map;
  }

  Map<String, dynamic> toMap() => {
        "entryId": entryId,
        "entryCreated": entryCreated,
        "entryTitle": entryTitle,
        "entryText": entryText
      };
}
