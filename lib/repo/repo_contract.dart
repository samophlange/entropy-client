import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:entropy_client/model/entry_model.dart';
import 'package:http/http.dart' as http;

//const String "178.128.139.88:3333";
const String SERVER_ADDRESS = "178.128.139.88:3333";

class RepoContract {
  Future<Entry> getEntry(int id) async {}
  Future<List<Entry>> getEntryList() async {}
  Future<bool> addEntry(Entry entry) async {}
  Future<bool> deleteEntry(int id) async {}
  Future<String> getEntryCount() async {}
}

class Repository implements RepoContract {
  @override
  Future<Entry> getEntry(int id) async {
    final response = await http.get(Uri.http(SERVER_ADDRESS, "/entry/get/$id"));
    if (response.statusCode == HttpStatus.ok) {
      log(response.body);
      return Entry.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Future<bool> deleteEntry(int id) async {
    final response =
        await http.delete(Uri.http(SERVER_ADDRESS, "/entry/delete/$id"));
    if (response.statusCode == HttpStatus.ok) {
      log("true");
      return true;
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Future<bool> addEntry(Entry entry) async {
    final http.Response response =
        await http.post(Uri.http(SERVER_ADDRESS, '/entry/create'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(entry));
    if (response.statusCode == HttpStatus.created) {
      log("true");
      return true;
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Future<List<Entry>> getEntryList() async {
    final response = await http.get(Uri.http(SERVER_ADDRESS, "/entry/list"));
    if (response.statusCode == HttpStatus.ok) {
      log(response.body);
      return (json.decode(response.body) as List)
          .map((data) => Entry.fromJson(data))
          .toList();
    } else {
      throw Exception('Failed to load album');
    }
  }

  // A function that converts a response body into a List<Entry>.
  List<Entry> parseEntryList(String responseBody) {
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<Entry>((json) => Entry.fromJson(json)).toList();
  }

  @override
  Future<String> getEntryCount() async {
    final response = await http.get(Uri.http(SERVER_ADDRESS, "/entry/count"));
    if (response.statusCode == HttpStatus.ok) {
      log(response.body);
      return response.body;
    } else {
      throw Exception('Failed to load album');
    }
  }
}
