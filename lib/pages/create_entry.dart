import 'package:entropy_client/model/entry_model.dart';
import 'package:entropy_client/repo/repo_contract.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CreateEntryRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Create an Entry'),
      ),
      body: EntryForm(),
    );
  }
}

class EntryForm extends StatefulWidget {
  @override
  _EntryFormState createState() => _EntryFormState();
}

class _EntryFormState extends State<EntryForm> {
  final _formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final bodyController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFormField(
            controller: titleController,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter a title.';
              }
              return null;
            },
          ),
          TextFormField(
            minLines: 3,
            maxLines: 6,
            maxLength: 200,
            controller: bodyController,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter your entry.';
              }
              return null;
            },
          ),
          Center(
            child: RaisedButton(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              onPressed: () {
                // Validate returns true if the form is valid, or false
                // otherwise.
                if (_formKey.currentState.validate()) {
                  // If the form is valid, display a Snackbar.
                  var _newEntry =
                      Entry.Create(titleController.text, bodyController.text);
                  Repository().addEntry(_newEntry);
                  Navigator.of(context).pop();
                }
              },
              child: Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}
