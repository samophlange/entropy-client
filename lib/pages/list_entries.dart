import 'package:entropy_client/model/entry_model.dart';
import 'package:entropy_client/pages/view_entry.dart';
import 'package:entropy_client/repo/repo_contract.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

// TODO: Make the network call to list the entries to navigate to them on click
class EntryListRoute extends StatefulWidget {
  @override
  State createState() => _EntryListState();
}

class _EntryListState extends State<EntryListRoute> {
  final GlobalKey<AnimatedListState> _listKey = GlobalKey();

  Future<List<Entry>> _futureEntryList;

  @override
  void initState() {
    super.initState();
    _futureEntryList = Repository().getEntryList();
  }

  Future<void> _showMyDialog(int index) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Warning!!!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Would you like to delete this entry?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Approve'),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  Repository().deleteEntry(index);
                  _futureEntryList = Repository().getEntryList();
                });
              },
            ),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text("No Way!"))
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Entry List'),
      ),
      body: _entryList(),
    );
  }

  /// A FutureBuilder that populates a ListView of entries some the webservice.
  Widget _entryList() {
    return FutureBuilder(
      future: _futureEntryList,
      initialData: List.empty(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        //show progress bar if no data
        if (snapshot.connectionState == ConnectionState.none &&
            !snapshot.hasData) {
          return Text('Fission Mailed');
        }
        return ListView.builder(
            key: _listKey,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              return ListTile(
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            ViewEntryRoute(snapshot.data[index]))),
                onLongPress: () => _showMyDialog(snapshot.data[index].entryId),
                title: Card(
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Text(snapshot.data[index].entryTitle),
                  ),
                ),
              );
            });
      },
    );
  }
}
