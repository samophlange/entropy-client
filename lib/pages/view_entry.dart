import 'package:entropy_client/model/entry_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

// TODO: Make the network call to show the entry and use the toolbar to nav back, bookmark, edit or delete
class ViewEntryRoute extends StatelessWidget {
  final Entry _entry;

  ViewEntryRoute(this._entry) {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Entry View'),
      ),
      body: Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                  child: Text(_entry.entryTitle, textAlign: TextAlign.start)),
              Expanded(
                  child: Text(
                _entry.entryCreated,
                textAlign: TextAlign.end,
              ))
            ],
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  _entry.entryText,
                  softWrap: true,
                  textAlign: TextAlign.start,
                  maxLines: 20,
                ),
              ),
            ],
          ),
        ],
      )),
    );
  }
}
